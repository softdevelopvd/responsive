$(document).ready(function(){
	//external
    $('a[rel="external"]').attr('target', '_blank');
	// slider banner
	$('.flexslider').flexslider({
		animation: "fade",
		direction: "horizontal",
		controlNav: false,
		mousewheel: false,
		directionNav: true
	 });
		// menu
	jQuery("ul.dropdownMenu li").hover(function(){
		jQuery(this).find('ul:first').css({visibility:"visible",display:"none"}).show(200);
		},function(){
		jQuery(this).find('ul:first').css({visibility:"hidden"});
	});
	/*Rule Hover*/
	var $timeMenuId = null;
	var menuCloseFlag = true;
	jQuery("ul.dropdownMenu ul.categorymenu").mouseout(function(){
		var self = this;
		menuCloseFlag = true;
		setTimeout(function(){
			if(menuCloseFlag){
				jQuery(self).parent().find("a:first").removeClass("hover-rule");
			}
		},400);	
	});
	jQuery("ul.dropdownMenu ul.categorymenu").mouseover(function(){
		menuCloseFlag = false;
		jQuery(this).parent().find("a:first").addClass("hover-rule");
	});
	jQuery("ul.dropdownMenu ul.categorymenu ul").mouseover(function(){
		menuCloseFlag = false;
		jQuery(this).parent().find("a:first").addClass("hover-rule-1");
		jQuery(this).parent().parent().parent().find("a:first").addClass("hover-rule");
	});
	jQuery("ul.dropdownMenu ul.categorymenu ul").mouseout(function(){
		var self = this;
		menuCloseFlag = true;
		setTimeout(function(){
			if(menuCloseFlag){
				jQuery(self).parent().find("a:first").removeClass("hover-rule-1");
				jQuery(self).parent().parent().parent().find("a:first").removeClass("hover-rule");
			}
		},200);	
	});
	//nav-sub-vertical
	var _navForRPW = $('.nav-vertical');
	var _buttonRPW = _navForRPW.find('a.button-nav');
	_buttonRPW.click(function(){
		$('ul.nav-sub-vertical').slideToggle();
		return false;
	});
	$( '.nav-vertical ul.nav-sub-vertical li.arrow-down > ul' )
		.hide()
		.click(function( e ){
			e.stopPropagation();
	});
	_buttonRPW.bind('click',function(e){
			e.stopPropagation();
			$(document).bind('click',function(){$('.nav-vertical ul.nav-sub-vertical').slideUp(500);});
		});
	$('.nav-vertical ul.nav-sub-vertical > li.arrow-down').toggle(function(){
		$(this).addClass('active-arrow');
		$(this).find('ul').slideDown(500);
	}, function(){
		$(this).find('ul').slideUp(500);
		$(this).rClass('active-arrow');
	});
	//nav-sub-vertical .end
	//popup top
	// $('#easytab').easytabs();
	
	$('.close-bt').live('click',function(e){
		$(".block-tab").fadeOut(1000);
		$('.popuptour,.popupquote').css({
			zIndex:21,
			paddingBottom:'10px'
		});
	});
	$('.popupquote').live('click',function(e){
		$("#pointer-2").show();
		$("#pointer-1").hide();
		$(".block-tab").fadeIn(1000);
		$(this).css({
			zIndex:22,
			paddingBottom:'20px'
		});
		$('.popuptour').css({
			zIndex:21,
			paddingBottom:'10px'
		});
	});
	$('.popuptour').live('click',function(e){
		$("#pointer-1").show();
		$("#pointer-2").hide();
		
		$(".block-tab").fadeIn(1000);
		$(this).css({
			zIndex:22,
			paddingBottom:'20px'
		});
		$('.popupquote').css({
			zIndex:21,
			paddingBottom:'10px'
		});
	});
	$("#form-ask-experts").validationEngine('attach', {
		onValidationComplete: function(form, status){
			if(status){
				alert("Thank you for your time. We will be in touch shortly.");
			}
				return true;
		}  
	});
	$("#form-request-callback").validationEngine('attach', {
		onValidationComplete: function(form, status){
			if(status){
				alert("Thank you for your time. We will be in touch shortly.");
			}
				return true;
		}  
	});
	jQuery(".main-form input[type=checkbox]").uniform();
});
function getCookie(c_name)
{
	var c_value = document.cookie;
	var c_start = c_value.indexOf(" " + c_name + "=");
	if (c_start == -1)
	{
		c_start = c_value.indexOf(c_name + "=");
	}
	if (c_start == -1)
	{
		c_value = null;
	} else {
		c_start = c_value.indexOf("=", c_start) + 1;
		var c_end = c_value.indexOf(";", c_start);
		if (c_end == -1)
		{
			c_end = c_value.length;
		}
		c_value = unescape(c_value.substring(c_start,c_end));
	}
	return c_value;
}

function setCookie(c_name,value,extime)
{
	var exdate=new Date();
	exdate.setDate(exdate.getTime() + extime);
	var c_value=escape(value) + ((extime==null) ? "" : "; expires="+exdate.toUTCString());
	document.cookie=c_name + "=" + c_value;
}

function eraseCookie(c_name) {
    setCookie(c_name,"",-1);
}